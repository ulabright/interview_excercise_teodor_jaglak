import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginTest {

    WebDriver driver;

    @BeforeEach
    public void setup(){

        driver = new ChromeDriver();
        //driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://github.com/login");
        driver.manage().window().maximize();
    }

    @Test
    void positiveLogin(){
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");

        //when
        driver.findElement(By.name("commit")).click();

        //then
        Assertions.assertEquals("Learn Git and GitHub without any code!", driver.findElement(By.xpath("/html/body/div[6]/div/div/div/div/div/main/div[1]/div/div/div/h3")).getText());
    }


    @Test
    void creatingIssue(){
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");
        driver.findElement(By.name("commit")).click();

        String issueTitle = "New issue: " + System.currentTimeMillis();
        String issueDescription = "New description: " + System.currentTimeMillis();

        //when
        driver.findElement(By.xpath("//*[@id=\"repos-container\"]/ul/li/div/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"issues-tab\"]/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"repo-content-pjax-container\"]/div/div[1]/div[2]/a/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"issue_title\"]")).sendKeys(issueTitle);
        driver.findElement(By.xpath("//*[@id=\"issue_body\"]")).sendKeys(issueDescription);
        driver.findElement(By.xpath("//*[@id=\"new_issue\"]/div/div/div[1]/div/div[1]/div/div[2]/button")).click();

        //then
        Assertions.assertEquals(issueTitle, driver.findElement(By.xpath("//*[@id=\"partial-discussion-header\"]/div[1]/div/h1/span[1]")).getText());

    }
    @Test
    void updatingIssue() {
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");
        driver.findElement(By.name("commit")).click();
        driver.findElement(By.xpath("//*[@id=\"repos-container\"]/ul/li/div/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"issues-tab\"]/span[1]")).click();

        //when
        driver.findElement(By.xpath("//*[@id=\"issue_6_link\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"partial-discussion-header\"]/div[1]/div/div/button")).click();

        driver.findElement(By.xpath("//*[@id=\"issue_title\"]")).sendKeys("Test");
        driver.findElement(By.xpath("//*[@id=\"edit_header_1059497966\"]/div/button[1]")).click();
    }

    @Test
    void addingComment(){
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");
        driver.findElement(By.name("commit")).click();
        driver.findElement(By.xpath("//*[@id=\"repos-container\"]/ul/li/div/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"issues-tab\"]/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"issue_6_link\"]")).click();

        //when
        driver.findElement(By.xpath("//*[@id=\"new_comment_field\"]")).sendKeys("New Comment");
        driver.findElement(By.xpath("//*[@id=\"partial-new-comment-form-actions\"]/div/div[2]/button")).click();
    }

    @Test
    void removeComment() throws InterruptedException {
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");
        driver.findElement(By.name("commit")).click();
        driver.findElement(By.xpath("//*[@id=\"repos-container\"]/ul/li/div/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"issues-tab\"]/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"issue_6_link\"]")).click();


        //when
        driver.findElements(By.cssSelector("[aria-label=\"Show options\"]")).get(1).click();
        Thread.sleep(1000L);
        driver.findElement(By.cssSelector("[aria-label=\"Delete comment\"]")).click();
        Thread.sleep(1000L);
        driver.switchTo().alert().accept();

    }
    @Test
    void assignment() throws InterruptedException {
        //given
        driver.findElement(By.id("login_field")).sendKeys("Teodorjaglak@gmail.com");
        driver.findElement(By.id("password")).sendKeys("haslohaslo123321");
        driver.findElement(By.name("commit")).click();
        driver.findElement(By.xpath("//*[@id=\"repos-container\"]/ul/li/div/div/a")).click();
        driver.findElement(By.xpath("//*[@id=\"issues-tab\"]/span[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"issue_6_link\"]")).click();

        //when
        driver.findElement(By.xpath("//*[@id=\"partial-discussion-sidebar\"]/div[1]/form/span/button")).click();
        Thread.sleep(2000L);
        driver.navigate().refresh();


        //then
        Assertions.assertEquals("Teo94j", driver.findElement(By.xpath("//*[@id=\"partial-discussion-sidebar\"]/div[1]/form/span/p/span/a[2]/span")).getText());
    }

    @AfterEach
    public void stop() {
        driver.quit();
    }
}
